section .data

section .text

%define EXIT_CODE 60
%define STDOUT 1
%define WRITE 1
%define NEW_LINE 0xA
%define TAB 0x9
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    cmp byte [rdi+rax], 0
    jz .end
    inc rax
    jmp .loop
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi
   call string_length
   pop rsi
   mov rdx, rax
   mov rdi, STDOUT
   mov rax, WRITE
   syscall
   ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, WRITE
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov rax, rdi
	mov r8, NEW_LINE
	mov r9, 0
	.loop:
	mov rdx, 0
	div r8
	add rdx, 0x30
	push rdx
	inc r9
	cmp rax, 0
	jnz .loop

	.delete:
	pop rdi
    push r9
	call print_char
    pop r9
	dec r9
	jnz .delete
	ret
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .num
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.num:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
        mov al, byte[rdi+rcx]
        mov ah, byte[rsi+rcx]
        cmp al,ah
        jnz .end1
        cmp al, 0
        jz .end2
	inc rcx
        jmp .loop
.end1:
        xor rax, rax
        ret
.end2:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    mov rsi, rsp
    xor rdi, rdi
    mov rdx, 1
    syscall 
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
.loop:
    push rdi
    push rsi
    push rcx
    call read_char
    pop rcx
    pop rsi
    pop rdi
    cmp rax, 0 
    jz .good
    cmp rcx, rsi
    jz .end
    cmp rcx, 0
    jz .first
.sign:
    cmp rax, ' '
    jz .good
    cmp rax, NEW_LINE
    jz .good
    cmp rax, TAB
    jz .good
    mov [rdi+rcx], rax
    inc rcx

    jmp .loop
.first:
    cmp rax, ' '
    jz .loop
    cmp rax, NEW_LINE
    jz .loop
    cmp rax, TAB
    jz .loop
	
    jmp .sign
.good:
    mov byte[rdi+rcx], 0
    mov rax, rdi
    mov rdx, rcx
    ret
.end:
    xor rax, rax
    ret 
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
  xor rax, rax
  xor rcx, rcx
  xor rdx, rdx

.loop:
  mov dl, byte[rdi + rcx]
  sub dl, 48
  cmp dl, 0
  js .end
  cmp dl, 10
  jns .end
  imul rax, 10
  add rax, rdx
  inc rcx
  jmp .loop
.end:
  mov rdx, rcx
  ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jnz .plus
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .plus:
    jmp parse_uint
    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .invalid
    push rax
    xor rcx, rcx
.loop:
    mov al, [rdi + rcx]
    mov [rsi + rcx], al
    inc rcx
    cmp al, 0
    je .end
    jmp .loop
.invalid:
    xor rax, rax
    ret
.end:
    pop rax
    ret
